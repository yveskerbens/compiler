BEGINPROG Evennumbers100

/* Print all the even numbers that are smaller than 100 */

VARIABLES i, j, k    /* Declare all the variables which
                               will be used in the program */
i := 0		//Give them the initial values
k := 1


FOR i := 0 TO 99 DO

	j := i + 2
	
	WHILE (k >= 1) DO
		k := j
		j := j - 2
	ENDWHILE
	
	IF (k = 0) THEN //good until here
		PRINT (i)
	ELSE
		PRINT (0)
	ENDIF
ENDFOR
ENDPROG