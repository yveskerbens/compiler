declare i32 @getchar()
define i32 @readVar() {
	 entry:		%res   = alloca i32
		%digit = alloca i32
		store i32 0, i32* %res
		br label %read
	 read:
		%0 = call i32 @getchar()
		%1 = sub i32 %0, 48
		store i32 %1, i32* %digit
		%2 = icmp ne i32 %0, 10
		br i1 %2, label %check, label %exit
	check:
		%3 = icmp sle i32 %1, 9
		%4 = icmp sge i32 %1, 0
%5 = and i1 %3, %4
		br i1 %5, label %save, label %exit
	save:
		%6 = load i32, i32* %res
		%7 = load i32, i32* %digit
		%8 = mul i32 %6, 10
		%9 = add i32 %8, %7
		store i32 %9, i32* %res
		br label %read
	exit:
		%10 = load i32, i32* %res
		ret i32 %10
}

@.strP = private unnamed_addr constant [4 x i8] c"%d\0A\00", align 1

; Function Attrs: nounwind uwtable
define void @println(i32 %x) #0 {
		%1 = alloca i32, align 4
		store i32 %x, i32* %1, align 4
		%2 = load i32, i32* %1, align 4
		%3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.strP, i32 0, i32 0), i32 %2)
		ret void
}

declare i32 @printf(i8*, ...) #1

define void @main(){
	entry:
		%number = alloca i32
		store i32 10, i32* %number
		%result = alloca i32
		store i32 1, i32* %result
		%test = alloca i32
		store i32 18, i32* %test
		%0 = load i32, i32* %number
		%1 = icmp sge i32 %0, 0
		br i1 %1, label %true0, label %false0
	true0:
		%2 = load i32, i32* %test
		%3 = sub i32 0, %2
		%4 = load i32, i32* %number
		%5 = mul i32 %4, 2
		%6 = add i32 %3, %5
		%i = alloca i32
		store i32 %6, i32* %i
		br label %forLoop0
	forLoop0:
		%7 = load i32, i32* %i
		%8 = icmp sle i32 %7, 5
		br i1 %8, label %doForLoop0, label %doneForLoop0
	doForLoop0:
		%9 = load i32, i32* %result
		%10 = load i32, i32* %i
		%11 = mul i32 %9, %10
		store i32 %11, i32* %result
		br label %endforLoop0
	endforLoop0:
		%12 = load i32, i32* %i
		%13 =subi32 %12, 1
		store i32 %13, i32* %i
		br label %forLoop0
	doneForLoop0:
		%14 = load i32, i32* %result
		call void @println(i32 %14)
		br label %end0
	false0:
		%15 = sub i32 0, 1
		call void @println(i32 %15)
		br label %end0
	end0:
		ret void
}
