package part3;


import part1.LexicalUnit;
import part2.Node;
import part2.Variable;
/**
 * This class is used to compute an arithmetic expression value 
 * it is used to implement the negative increment solution
 * by taking in input the expression and return an integer
 * thus  the value of fromExpression and toExpression of the for loop can be compared 
 * if the former one is bigger than the later one, a sub operation will be done 
 * otherwise an add-operation is performed
 *
 */
public class ForTool {
	private static  int counter;
/**
 * The method takes 
 * @param node 
 *as input  and 
 * @return result
 *
 */
	 public static int getComputedValue(Node node){
	        switch (node.GetSententialForm().getV()){
	            case ExprArith:
	            	int result1;
	                Node Prod=node.GetSpecificChild(0);
	                Node  ExprAP=node.GetSpecificChild(1);
	            	result1 = getComputedValue(Prod);
	                while( ExprAP.GetNumberofChildren()>0){
	                	Node OpG1= ExprAP.GetSpecificChild(0);
	                    Prod= ExprAP.GetSpecificChild(1);
	                    if(OpG1.GetSpecificChild(0).GetTerminal().toString().equals("+")) {
	                    	result1 += getComputedValue(Prod);
	                    } else {
	                    	result1 -= (getComputedValue(Prod));
	                    }
	                    
	                    ExprAP= ExprAP.GetSpecificChild(2);
	                }
	               return result1;

	            case Prod:
	            	int result2;
	                Node Atom=node.GetSpecificChild(0);
	                Node ProdP=node.GetSpecificChild(1);
	            	result2 = getComputedValue(Atom);
	                while(ProdP.GetNumberofChildren()>0){
	                	Node OpG2= ProdP.GetSpecificChild(0);
	                    if(OpG2.GetSpecificChild(0).GetTerminal().toString().equals("*")) {
	                    	result2 *= getComputedValue(Atom);
	                    } else {
	                    	try {
	                    		result2 /= getComputedValue(Atom);
	                    	} catch(ArithmeticException e) {
	                    		System.out.println(e);
	                    	}    	
	                    }
	                    
	                    getComputedValue(Atom);
	                    ProdP=ProdP.GetSpecificChild(2);
	 
	                }
	               
	                return result2;
	            case Atom:
	            	if(!node.GetSententialForm().getV().equals(Variable.Atom)) {
	            		break;
	            	}
	            	
	            	 Node expression;
	            	    int value;
		                switch(node.GetNumberofChildren()) {
		                    
		                    case 1:   //VarName or Number
		                        expression = node.GetSpecificChild(0);
		                        if(expression.GetSententialForm().getT() == LexicalUnit.NUMBER) {
		                        	 value= Integer.parseInt(expression.GetTerminal().toString());
		                           }
		                        else
		                        {  
		                        	/*return a given variable stored value Variable value is stored in the 
		                        	  varNamValues dictionary during assignment */
		                        	 value = Integer.parseInt(Interpretor.varNamValues.get(expression.GetTerminal().toString())); 
		                        	 
		                        }
		                        return value;
		                    case 2: //negative expression
		                         expression = node.GetSpecificChild(1);
		                         value = getComputedValue(expression);
		                         return -value;
		                    case 3: //complex atom
		                        expression = node.GetSpecificChild(1);
		                        value = getComputedValue(expression);
		                        return value; 
		                      
		               }
	            default:
	            	return 0;
	              
	        }
			return 0;
	    }
}
