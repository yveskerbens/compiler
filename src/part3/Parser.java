package part3;
import java.util.Queue;
import java.util.Stack;
import part1.LexicalUnit;
import part1.Symbol;
import part2.ActionTable;
import part2.Grammar;
import part2.ParseTree;
import part2.ProductionRule;
import part2.SententialForm;
import part2.Variable;

public class Parser {
	private static Grammar grammar;
	private static ActionTable  actionTable;
	public static Boolean accepted=false;
	private static Stack<SententialForm> stack=new Stack<SententialForm>();
	private static Stack<Integer> stackRule=new Stack<Integer>();  
	private static Stack<Integer> stackDepth=new Stack<Integer>();

	//Pushdown automaton
    public static ParseTree tree=new ParseTree(new SententialForm(Variable.Program)); //tree structure for llvm output

/**
 * 
 * 
 * @param symbolTable the list of  tokens
 * 
 * 
 */
		public static void action(Queue<Symbol> symbolTable, String verbose){
			stack.push(new SententialForm(Variable.Program));
			stackRule.push(1);
			stackDepth.push(0);
			while(true)
			{
				if(tree.isFinish() && symbolTable.isEmpty()) 
				{  
					System.out.println();
					System.out.println("Program accepted");
					accepted=true;
					return;
				}
				else if(tree.isFinish() || symbolTable.isEmpty())
				{
					System.out.println("Program Error!");
					return;
				}
				else{
					stack.pop();
					Integer topRule=stackRule.pop();
					Integer elementDepth= stackDepth.pop(); //get the element depth
					SententialForm topT=tree.GetChildNode().GetSententialForm(); //define the tree
					Symbol topS=symbolTable.peek();
		
					//Producing rules
				    if(topT.getType().equals("Variable")){
				    	Variable topV=topT.getV(); //value at top of stack
				    	int r= ActionTable.getProductionRule(topV,topS.getType());
				    	if(r!=-1)
				    	{
				    		SententialForm[] strp = grammar.getProductionRuleList().get(r).getRightPart();
				    		for(int i=strp.length-1;i>=0;--i)
				    		{
				    			stack.push(grammar.getProductionRuleList().get(r).getRightPart()[i]);
				    			stackRule.push(r);
				    			stackDepth.push(elementDepth+1);
				    		}
				    		// print spaces for indicating rule depth.
							
							if(verbose == "-v" ) {
				    			verbose(topS,  topV, null, r, topRule, elementDepth);
				    		} else {
				    			if(elementDepth!=0) {
									for (int idx = 1; idx <= elementDepth; ++idx) {
										System.out.print(" ");
									}
								}
				    			System.out.print(r);
				    		}
							tree.addChildrenNode(strp, verbose); //producting rules
				    	}
				    	else
				    	{
				    		if(verbose == "-v" ) {
				    			verbose(topS,  topV, null, r, topRule, elementDepth);
				    		}
				    		return;
				    	}	
				    	
				    	
				    }
				    //Match
				    else
				    {
				    	LexicalUnit topL=topT.getT();
				    	if(topL==topS.getType()) {
							tree.SetTerminalValue(topS.getValue());
							symbolTable.poll();
							
						} 
				    	else if (topL == LexicalUnit.EPSILON) {
				    	
				    	}
				    	else
				    	{
				    		if(verbose == "-v" ) {
				    			verbose(topS,  null, topL , 0 , topRule, elementDepth);
				    		}
				    		
				    		 return;
				    	}
				    }
				    tree.findNextChildren(verbose); 
				}
			}
		}
	//End of parser
		
		public static void verbose(Symbol topS,  Variable topV, LexicalUnit topT, int  ruleNum, int topRule, int elementDepth) {
			System.out.println("\n");
			Object v = topV;
    		Object ts = topS;
			if (topV!=null)  {
				if(ruleNum!=-1){
					System.out.println("Variable at top of the stack: " + "<"+ v.toString() +"> "
							+ "---> it's first(follow) terminal is: " + "["+ ts.toString() +"]");
					System.out.print("The Produced Rule:\n\t\t ["+ ruleNum+"] --> ");
					grammar.getProductionRuleList().get(ruleNum).displayRule(elementDepth);
					System.out.println();		
				}else {
					System.out.println("ERROR: The variable<"+  v.toString() +"> "
		    				+ "is unable to produce with terminal " + "["+  ts.toString() +"] as first or follow");
		    		System.out.print("Variable related Error occured at ProductionRule ["+topRule+"]: ");
		    		grammar.getProductionRuleList().get(topRule).displayRule(elementDepth);	
				}
				
			} else {
				Object t = topT;
	    		System.out.println("No match for the ["+ t.toString() +"] terminal");
	    	    System.out.print("Error occured at ProductionRule ["+topRule+"]: ");
	    	    grammar.getProductionRuleList().get(topRule).displayRule(elementDepth);
			}
		}
		
		public static void parser(Queue<Symbol> symbolTable, String verbose)
		{
			grammar = new Grammar();
			actionTable = new ActionTable();
			action(symbolTable, verbose);
		}
		
	
}



