package part3;
import part1.LexicalUnit;
import part2.Node;
public class Arithmetic {
	
	 public static String  compute(Node node){
	        switch (node.GetSententialForm().getV()){
	            case ExprArith:
	                String computedVal1,computedVal2;
	                Node Prod=node.GetSpecificChild(0);
	                Node  ExprAP=node.GetSpecificChild(1);
	                computedVal1=compute(Prod);
	                while( ExprAP.GetNumberofChildren()>0){
	                    Node OPG1= ExprAP.GetSpecificChild(0);
	                    Node operator= OPG1.GetSpecificChild(0);
	                    Prod= ExprAP.GetSpecificChild(1);
	                    computedVal2=compute(Prod);
	                    ExprAP= ExprAP.GetSpecificChild(2);
	                    System.out.println("\t\t%"+Interpretor.variableCounter+" = "+getOperator(operator)
	                            +" i32 "+computedVal1+", "+computedVal2);
	                    computedVal1 = "%"+Integer.toString(Interpretor.variableCounter++);
	                }
	                return computedVal1;

	            case Prod:
	                String computedVal3,computedVal4;
	                Node Atom=node.GetSpecificChild(0);
	                Node ProdP=node.GetSpecificChild(1);
	                computedVal3=compute(Atom);
	                while(ProdP.GetNumberofChildren()>0){
	                	Node OpG2=ProdP.GetSpecificChild(0);
	                    Node operator=OpG2.GetSpecificChild(0);
	                    Atom=ProdP.GetSpecificChild(1);
	                    computedVal4=compute(Atom);
	                    ProdP=ProdP.GetSpecificChild(2);
	                    System.out.println("\t\t%"+Interpretor.variableCounter+" = "+getOperator(operator)
	                            +" i32 "+computedVal3+", "+computedVal4);
	                    computedVal3 = "%"+Integer.toString(Interpretor.variableCounter++);

	                }
	                return computedVal3;
	            case Atom:
	                String value;
	                Node expression;
	                switch(node.GetNumberofChildren()) {
	                    case 1:   //[VarName] or [Number]
	                        expression = node.GetSpecificChild(0);
	                        if(expression.GetSententialForm().getT() == LexicalUnit.NUMBER)
	                            value = expression.GetTerminal().toString();
	                        else
	                        {
	                            System.out.println("\t\t%"+Interpretor.variableCounter+" = load i32, i32* %"+expression.GetTerminal());
	                            value = "%"+Integer.toString(Interpretor.variableCounter++);
	                        }
	                        return value;
	                    case 2:   
	                        expression = node.GetSpecificChild(1);
	                        String numE = compute(expression);
	                        System.out.println("\t\t%"+Interpretor.variableCounter+" = sub i32 0, "+numE);
	                        value = "%"+Integer.toString(Interpretor.variableCounter++);
	                        return value;
	                    case 3:	  
	                        expression = node.GetSpecificChild(1);
	                        return compute(expression);
	                }
	                
	            case Cond:
	                String computedVal5,computedVal6;
	                Node CondBlock=node.GetSpecificChild(0);
	                Node CondP=node.GetSpecificChild(1);
	                computedVal5=compute(CondBlock);
	                while(CondP.GetNumberofChildren()>0){
	                    Node operator2=CondP.GetSpecificChild(0);
	                    CondBlock=CondP.GetSpecificChild(1);
	                    CondP=CondP.GetSpecificChild(2);
	                    computedVal6=compute(CondBlock);
	                    System.out.println("\t\t%"+Interpretor.variableCounter+" = "+getOperator(operator2)
	                            +" i1 "+computedVal5+", "+computedVal6);
	                    computedVal5 = "%"+Integer.toString(Interpretor.variableCounter++);
	                }
	                return computedVal5;
	            case CondBlock:
	                String computedVal7,computedVal8;
	                Node CondAtom=node.GetSpecificChild(0);
	                Node CondBlockP=node.GetSpecificChild(1);
	                computedVal7=compute(CondAtom);
	                while(CondBlockP.GetNumberofChildren()>0){
	                    Node operator3=CondBlockP.GetSpecificChild(0);
	                    CondAtom=CondBlockP.GetSpecificChild(1);
	                    CondBlockP=CondBlockP.GetSpecificChild(2);
	                    computedVal8=compute(CondAtom);
	                    System.out.println("\t\t%"+Interpretor.variableCounter+" = "+getOperator(operator3)
	                            +" i1 "+computedVal7+", "+computedVal8);
	                    computedVal7 = "%"+Integer.toString(Interpretor.variableCounter++);
	                }
	                return computedVal7;
	            case CondAtom:
	                if(node.GetNumberofChildren()==1){
	                    Node simpleCond=node.GetSpecificChild(0);
	                    return compute(simpleCond);
	                }
	                else{
	                    Node simpleCond=node.GetSpecificChild(1);
	                    String computedVal9=compute(simpleCond);
	                    System.out.println("\t\t%"+Interpretor.variableCounter+" = "+"sub i1 1"+", "+computedVal9);
	                    return  "%"+Integer.toString(Interpretor.variableCounter++);

	                }
	            case SimpleCond:
	                String computedVal11,computedVal12;
	                Node ExprArith=node.GetSpecificChild(0);
	                Node compOperator=node.GetSpecificChild(1).GetSpecificChild(0);
	                Node ExprArith2=node.GetSpecificChild(2);
	                computedVal11=compute(ExprArith);
	                computedVal12=compute(ExprArith2);
	                System.out.println("\t\t%"+Interpretor.variableCounter+" = "+getOperator(compOperator)
	                        +" i32 "+computedVal11+", "+computedVal12);
	                return  "%"+Integer.toString(Interpretor.variableCounter++);
	            default:
	                return null;
	        }
	    }

		public static String getOperator(Node operator)
	    {
	        switch(operator.GetSententialForm().getT())
	        {   
	            case TIMES: return "mul";
                case DIVIDE: return "sdiv";
	            case PLUS: return "add";
	            case MINUS: return "sub";
	            case AND: return "and";
	            case OR: return "or";
	            case EQ: return "icmp eq";
	            case NEQ: return "icmp ne";
	            case GEQ: return "icmp sge";
	            case GT: return "icmp sgt";
	            case LEQ: return "icmp sle";
	            case LT: return "icmp slt";
	            default:
	                return null;
	        }
	    }

}
