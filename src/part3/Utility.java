package part3;

public class Utility {
	public static void utilityFunction() {
        System.out.println("declare i32 @getchar()");

        System.out.print(
        		"define i32 @readVar() {\n"+
                "\t entry:"+
                "\t\t%res   = alloca i32\n"+
                "\t\t%digit = alloca i32\n"+
                "\t\tstore i32 0, i32* %res\n"+
                "\t\tbr label %read\n"+
                "\t read:\n"+
                "\t\t%0 = call i32 @getchar()\n"+
                "\t\t%1 = sub i32 %0, 48\n"+
                "\t\tstore i32 %1, i32* %digit\n"+
                "\t\t%2 = icmp ne i32 %0, 10\n"+
                "\t\tbr i1 %2, label %check, label %exit\n"+
                "\tcheck:\n"+
                "\t\t%3 = icmp sle i32 %1, 9\n"+
                "\t\t%4 = icmp sge i32 %1, 0\n"+
                "%5 = and i1 %3, %4\n"+
                "\t\tbr i1 %5, label %save, label %exit\n"+
                "\tsave:\n"+
                "\t\t%6 = load i32, i32* %res\n"+
                "\t\t%7 = load i32, i32* %digit\n"+
                "\t\t%8 = mul i32 %6, 10\n"+
                "\t\t%9 = add i32 %8, %7\n"+
                "\t\tstore i32 %9, i32* %res\n"+
                "\t\tbr label %read\n"+
                "\texit:\n"+
                "\t\t%10 = load i32, i32* %res\n"+
                "\t\tret i32 %10\n"+
                "}\n\n");


        System.out.println("@.strP = private unnamed_addr constant [4 x i8] c\"%d\\0A\\00\", align 1\n");
        System.out.println("; Function Attrs: nounwind uwtable");

        System.out.print("define void @println(i32 %x) #0 {\n"+
                "\t\t%1 = alloca i32, align 4\n"+
                "\t\tstore i32 %x, i32* %1, align 4\n"+
                "\t\t%2 = load i32, i32* %1, align 4\n"+
                "\t\t%3 = call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.strP, i32 0, i32 0), i32 %2)\n"+
                "\t\tret void\n"+
                "}\n\n");

        System.out.println("declare i32 @printf(i8*, ...) #1\n");
    }

}
