package part3;

import java.util.Iterator;

import part2.Node;

public class Read {
	public static void  getVarList(Node varList) {
    	switch(varList.GetSententialForm().getV()) {
    	     case VarList:
    	    	 Node varNameToRead =  varList.GetSpecificChild(0);
    	    	 Node VarListVarListP=   varList.GetSpecificChild(1);
    	    	 String varToRead = varNameToRead.GetTerminal().toString();
    	    	 if(!Interpretor.varNameList.contains(varToRead  )) {
    	    		 System.out.println("\t\t%"+varToRead+" = alloca i32");
    	    		 Interpretor.varNameList.add(varToRead);
    	    	 }
    	    	 
    	    	 System.out.println("\t\t%"+Interpretor.variableCounter+" = call i32 @readVar()");
                 System.out.println("\t\tstore i32 %"+(Interpretor.variableCounter++)+", i32* %"+varToRead);
    	    	 if(VarListVarListP.GetNumberofChildren() > 0) {
    	    		 VarListVarListP.getNodeChildren().remove(0); //removing the comma
                     Iterator<Node> it = VarListVarListP.getNodeChildren().iterator(); //get the expression value
    	    		 Node VarList =  it.next();
    	    		 getVarList(VarList);
    	    	 }
    	    	 break;
    	    	 
   		     default:
   		    	break;
   		}
    }
}
