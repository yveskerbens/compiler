package part3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import part2.Node;

public class Interpretor {
    
    public static int variableCounter = 0;
	private static int ifCounter=0;
    private static int whileCounter=0;
    private static int forCounter=0;
    public static ArrayList<String> varNameList = new ArrayList<String>(); 
    public static HashMap<String, String> varNamValues = new HashMap<String, String> () ; //used to store assigned (varname, value) pair
   
 
    public static void genLLVM(Node node){
        switch(node.GetSententialForm().getV()){
            case EndLine:
            	Node ProgramNode = node.GetSpecificChild(1);
            	 genLLVM(ProgramNode);
        
            case Program:
            	Utility.utilityFunction();
                Node variableNode = node.GetSpecificChild(4);
                Node codeNode = node.GetSpecificChild(5);
                System.out.println("define void @main(){");
                System.out.println("\tentry:");
                genLLVM(variableNode);
                genLLVM(codeNode);
                System.out.println("\t\tret void");
                System.out.println("}");
                break;
                
            case Variables:
            	if(node.GetNumberofChildren()==0)break;
                Node varListNode=node.GetSpecificChild(1);
                genLLVM(varListNode); 
                
            case VarList:
                Node VarListPNode=node.GetSpecificChild(0);
                if(VarListPNode.GetNumberofChildren()>0){
                    Node VarList=VarListPNode.GetSpecificChild(1);
                    genLLVM(VarList);
                }
                break;
                
            case Code:
                if(node.GetNumberofChildren()==0)break;
                Node InstructionNode=node.GetSpecificChild(0);
                genLLVM(InstructionNode);
                Node CodeNode=node.GetSpecificChild(3);
                genLLVM(CodeNode);
                break;
                
            case Instruction:
                Node action=node.GetSpecificChild(0);
                genLLVM(action);
                break;
            case Assign:
                String varName= node.GetSpecificChild(0).GetTerminal().toString();
                Node expArth=node.GetSpecificChild(2);
                String varValue=Arithmetic.compute(expArth);
                if(!varNameList.contains(varName)){
                    System.out.println("\t\t%"+varName+" = alloca i32");
                    varNameList.add(varName);
                }
                varNamValues.put(varName, varValue); //store varName value as part of for negative increment solution
                System.out.println("\t\tstore i32 "+varValue+", i32* %"+varName);
                break;
            case If:
                Node condition=node.GetSpecificChild(2);
                Node codeIf=node.GetSpecificChild(7);
                Node ifSeq=node.GetSpecificChild(8);
                String varValue2=Arithmetic.compute(condition);
                int localifCounter=ifCounter++;
                System.out.println("\t\tbr i1 "+varValue2+", label %true"+localifCounter+
                        ", label %false"+localifCounter);
                //if condition is true
                System.out.println("\ttrue"+localifCounter+":");
                genLLVM(codeIf);
                System.out.println("\t\tbr label %end"+localifCounter);
                //if condition is false
                System.out.println("\tfalse"+localifCounter+":");
                genLLVM(ifSeq);
                System.out.println("\t\tbr label %end"+localifCounter);
                System.out.println("\tend"+localifCounter+":");
                break;
            case IffSeq:
                if(node.GetNumberofChildren()==1){
                    break;
                }
                else{
                    Node elseCode=node.GetSpecificChild(3);
                    genLLVM(elseCode);
                    break;
                }
            case While:
                Node whileCond=node.GetSpecificChild(2);
                Node whileCode=node.GetSpecificChild(7);
                int localwhileCounter=whileCounter++;
                System.out.println("\t\tbr label %whileLoop"+localwhileCounter);
                System.out.println("\twhileLoop"+localwhileCounter+":");
                String varValue3=Arithmetic.compute(whileCond);

                System.out.println("\t\tbr i1 "+varValue3+", label %whileCondTrue"+localwhileCounter+
                        ", label %whileCondFalse"+localwhileCounter);
                //while condition is true
                System.out.println("\twhileCondTrue"+localwhileCounter+":");
                genLLVM(whileCode);
                System.out.println("\t\tbr label %whileLoop"+localwhileCounter);
                //while condition is false
                System.out.println("\twhileCondFalse"+localwhileCounter+":");
                break;

            case For:
                String increaseVar, terminateVar;
                Node forCode;
                String forVarname=node.GetSpecificChild(1).GetTerminal().toString();
                Node fromExpArth=node.GetSpecificChild(3);
                String varValue1=Arithmetic.compute(fromExpArth);
                Boolean restoreValue = false;
                int valueBefore = variableCounter;
                if(!varNameList.contains(forVarname)){
                    System.out.println("\t\t%"+forVarname+" = alloca i32");
                    varNameList.add(forVarname);

                 }
                 else{
                    System.out.println("\t\t%"+valueBefore+" = load i32, i32* %"+forVarname);
                    restoreValue = true;
                    variableCounter++;
                }

                System.out.println("\t\tstore i32 "+varValue1+", i32* %"+forVarname);
               
                
                Node toExpArth=node.GetSpecificChild(5);
                forCode=node.GetSpecificChild(9);
                increaseVar="1";
                terminateVar=Arithmetic.compute(toExpArth);
    
                int from = ForTool.getComputedValue(fromExpArth); //negative increment from expression
                int to = ForTool.getComputedValue(toExpArth);// negative increment to expression
        
                if(from <= to) {
                	 computeFor("add", forCode, forVarname,  terminateVar, increaseVar);
                } else {
                	computeFor("sub", forCode, forVarname,  terminateVar, increaseVar);
                }
                		
                if(restoreValue){
                    System.out.println("\t\tstore i32 %"+valueBefore+", i32* %"+forVarname);
                }
                break;
            case Print:
            	Node printExpList = node.GetSpecificChild(2);
            	genLLVM(printExpList);
                break;
            
            case ExpList:
            	Node ExpListExprArith = node.GetSpecificChild(0);
            	Node ExpListExpListP = node.GetSpecificChild(1);
            	String varToPrint = Arithmetic.compute(ExpListExprArith);
            
                System.out.println("\t\tcall void @println(i32 "+ varToPrint+ ")");
                
            	if( ExpListExpListP.GetNumberofChildren() > 0) {
            		ExpListExpListP.getNodeChildren().remove(0); //removing the comma
                    Iterator<Node> it = ExpListExpListP.getNodeChildren().iterator(); //get the expression value 
            		Node ExpList = it.next();
            		genLLVM(ExpList);	
            	}
            	
                break;
          
            case Read:
            	Node readVarName = node.GetSpecificChild(2);
            	Read.getVarList(readVarName);
                break; 

            default:
                break;

        }
    }
    
    public  static void computeFor(String operation, Node forCode, String forVarname, String terminateVar, String increaseVar) {
    	int localforCounter=forCounter++;
    	System.out.println("\t\tbr label %forLoop"+localforCounter);
        System.out.println("\tforLoop"+localforCounter+":");
        System.out.println("\t\t%"+variableCounter+" = load i32, i32* %"+forVarname);
        System.out.println("\t\t%"+(variableCounter+1)+" = icmp sle i32 %"+variableCounter+
                ", "+terminateVar);
        System.out.println("\t\tbr i1 %"+(variableCounter+1)+", label %doForLoop"+localforCounter+", label %doneForLoop"+localforCounter);
        variableCounter+=2;
        //execute the do code
        System.out.println("\tdoForLoop"+localforCounter+":");
        genLLVM(forCode);
        System.out.println("\t\tbr label %endforLoop"+localforCounter);
        //increase forVarname value
        System.out.println("\tendforLoop"+localforCounter+":");
        System.out.println("\t\t%"+variableCounter+" = load i32, i32* %"+forVarname);
        System.out.println("\t\t%"+(variableCounter+1)+" =" + operation + "i32 %"+variableCounter+", "+increaseVar);
        System.out.println("\t\tstore i32 %"+(variableCounter+1)+", i32* %"+forVarname);
        System.out.println("\t\tbr label %forLoop"+localforCounter);
        variableCounter+=2;
        System.out.println("\tdoneForLoop"+localforCounter+":");
    	
    }
    
}

