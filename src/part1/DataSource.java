package part1;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
/**
 * This is a DATA BASE Access class. Used to access data 
 * The tokens of  are stored in a database using mysqlite
 * @author yveskerbensdeclerus
 * it's a singleton
 *
 */
public class DataSource {
	private static final  String DB_NAME = "tokensList.db";
	private static final String url = "jdbc:sqlite:./more/" + DB_NAME;
	private static final String DEL_DATA = "DELETE FROM Tokens";
	private static final String INSERT_DATA = "INSERT INTO Tokens (type, yyline, yycolumn, value) VALUES (?, ?, ?, ?);";
	
	private static DataSource instance;
	Connection conn; 
	PreparedStatement prstmt; 
	

	private DataSource() {
		try {
			this.conn =  DriverManager.getConnection(url);
			this.prstmt = conn.prepareStatement("CREATE TABLE IF NOT EXISTS Tokens(type varchar(30), yyline int, yycolumn int, value varchar(100))");
			prstmt.execute();
		} catch (SQLException e) {
			System.out.println("Failed to create DB");
		}
	}
	
	/**
	 * This method initialize the data based by removing all the data. 
	 */
	public void initializeTokensTable() {
		try {
			conn = DriverManager.getConnection(url);
			prstmt = this.conn.prepareStatement(DEL_DATA );
			prstmt.execute();
			System.out.println("The tokens table is succesfull initialized");
			}catch(SQLException e) {
				System.out.println("Failed to delete data: " + e.getMessage());
			} finally {
				try {
					if(prstmt != null) {
						prstmt.close();
					}

					if(conn != null) {
						conn.close();
					}
				} catch(SQLException e) {
					System.out.println(e.getMessage());
				}
			}
	}
	
	/**
	 * The open method opens a connection to the database 
	 * @return
	 */
	public boolean open() {
		try {
			conn = DriverManager.getConnection(url);
			return true;
		} catch(SQLException e ) {
			System.out.println("Connection failed: " + e.getMessage());
			return false;
		}
	}
	
	public void close() {
		try {
			if(conn != null ) {
				conn.close();
				} 
			} catch(SQLException e ) {
				System.out.println(e.getMessage());
				}
		}
	
	public static synchronized DataSource getInstance() {
		if(instance == null ) {
			instance =  new DataSource();
		}
		return instance;
	}

	
	public Connection getConn() {
		return conn;
	}
	
	/**
	 * This method is called in the lexer and takes and object of type Symbol argument to store it in the database
	 * @param toSave is the token of type Symbol to be saveed
	 * @return
	 */
	public boolean saveToken(Symbol toSave) {
  		try {
  			conn =  DriverManager.getConnection(url);
  			prstmt = this.conn.prepareStatement(INSERT_DATA);
            prstmt.setString(1, toSave.getType().name());
  			prstmt.setInt(2, toSave.getLine());
            prstmt.setInt(3, toSave.getColumn());
  			prstmt.setString(4, (String)toSave.getValue());
  			prstmt.executeUpdate();
  			//System.out.println("Data succesfully saved");
  			return true;

  		} catch(SQLException e) {
  			System.out.println("Failed to add token: " + e.getMessage());
  			return false;
  		} finally {
			try {
				if(prstmt != null) {
					prstmt.close();
				}

				if(conn != null) {
					conn.close();
				}
			} catch(SQLException e) {
				System.out.println(e.getMessage());
			}
		}
  }
	
	/**
	 * This method will save all the generated tokens in a data base
	 * @return a list of all matched tokens
	 */
	
	/**
	 * This method queries the database to retrieve the tokens
	 * @return an ArrayList of tokens
	 */
	public  Queue<Symbol> getSymbolTable() {
		Queue<Symbol> SymbolTable = new LinkedList<Symbol>();
       	PreparedStatement prstmt = null;
       	try {
            conn = DriverManager.getConnection(url);
       		prstmt = conn.prepareStatement("select * from Tokens");
       		ResultSet results = prstmt.executeQuery();
       		while(results.next()) { 
       			String lu = results.getString("type");
       			Symbol token = new Symbol(LexicalUnit.valueOf(lu), results.getInt("yyline"), results.getInt("yycolumn"), (Object)results.getString("value"));
       			SymbolTable.add(token);
       		}
       		
       		}  catch(SQLException e) {
        	   System.out.println(e.getMessage());
        	   }
       	return SymbolTable;
       	} 
}