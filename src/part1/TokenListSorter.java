package part1;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * This class is an utility class whose instance will be used to sort the tokens in alphabetic orders
 * @author yveskerbensdeclerus
 */

public class TokenListSorter{
	private DataSource  ds = DataSource.getInstance(); //creating an instance of the DAO class to access the database
	private ArrayList<Symbol> tokenList = new ArrayList<Symbol>();
	
	public TokenListSorter(ArrayList<Symbol> tokenList) {this.tokenList = tokenList;}
	
	/**
	 * this method is used to sort the tokens in alphabetic order.
	 * @return an ordered list of tokens
	 */
	public ArrayList<Symbol> getSortedSymbolList(){
		Collections.sort(tokenList, sortTokenValue);
		return tokenList;
	}
	
	public static Comparator<Symbol> sortTokenValue = new Comparator<Symbol>() {

		@Override
		public int compare(Symbol tk1, Symbol tk2) {
			return (int) (((String) tk1.getValue()).compareTo((String)tk2.getValue()));
		}	
	};
	
}