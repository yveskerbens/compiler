package part1;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Queue;

%%// Options of the scanner

%public
%class LexicalAnalyzer//Name
%unicode			//Use unicode
%line         //Use line counter (yyline variable)
%column       //Use character counter by line (yycolumn variable)
%type Symbol
%standalone		//Standalone mode


//general state and comment state

//java execution code
%{
	public LexicalAnalyzer() {}

	public Symbol  symbol(Symbol S){
		DataSource  ds = DataSource.getInstance();
		//Add symbol into symbol table
		if(S!=null){
			ds.saveToken(S);
		}
		System.out.println(S);
		return S;
	}
%}


AlphaUpperCase = [A-Z]
AlphaLowerCase = [a-z]
Alpha          = {AlphaUpperCase}|{AlphaLowerCase}
Numeric        = [0-9]
AlphaNumeric   = {Alpha}|{Numeric}
AlphaLowNumeric  = {AlphaLowerCase}|{Numeric}
EndLine        = "\n"?
Integer        = (([1-9][0-9]*)|0)
Identifier     = {AlphaLowerCase}({AlphaNumeric}*)
ShortComment   = "//".*
LongComment    = "/*"({AlphaNumeric}*{EndLine}*[^[*/]])*"*/"
Comment       = {ShortComment}|{LongComment}
programname    = {AlphaUpperCase}({AlphaLowNumeric}{AlphaUpperCase}?)*



%%
//comment
{Comment } {}
{programname} {return symbol(new Symbol(LexicalUnit.PROGNAME,yyline,yycolumn,yytext()));}
"+"	{return symbol(new Symbol(LexicalUnit.PLUS,yyline,yycolumn,yytext()));}
"-"	{return symbol(new Symbol(LexicalUnit.MINUS,yyline,yycolumn,yytext()));}
"*"	{return symbol(new Symbol(LexicalUnit.TIMES,yyline,yycolumn,yytext()));}
"/"	{return symbol(new Symbol(LexicalUnit.DIVIDE,yyline,yycolumn,yytext()));}

//Relational operators
">="	{return symbol(new Symbol(LexicalUnit.GEQ,yyline,yycolumn,yytext()));}
">"		{return symbol(new Symbol(LexicalUnit.GT,yyline,yycolumn,yytext()));}
"<="	{return symbol(new Symbol(LexicalUnit.LEQ,yyline,yycolumn,yytext()));}
"<"		{return symbol(new Symbol(LexicalUnit.LT,yyline,yycolumn,yytext()));}
"="		{return symbol(new Symbol(LexicalUnit.EQ,yyline,yycolumn,yytext()));}
"<>"	{return symbol(new Symbol(LexicalUnit.NEQ,yyline,yycolumn,yytext()));}

//Program control
"BEGINPROG"  {return symbol(new Symbol(LexicalUnit.BEGINPROG,yyline,yycolumn,yytext()));}
"ENDPROG"	{return symbol(new Symbol(LexicalUnit.ENDPROG,yyline,yycolumn,yytext()));}
"VARIABLES" {return symbol(new Symbol(LexicalUnit.VARIABLES,yyline,yycolumn,yytext()));}


":="  {return symbol(new Symbol(LexicalUnit.ASSIGN,yyline,yycolumn,yytext()));}

//control symbols
"("	{return symbol(new Symbol(LexicalUnit.LPAREN,yyline,yycolumn,yytext()));}
")"	{return symbol(new Symbol(LexicalUnit.RPAREN,yyline,yycolumn,yytext()));}
","	{return symbol(new Symbol(LexicalUnit.COMMA,yyline,yycolumn,yytext()));}

//Line symbols
{EndLine} {return symbol(new Symbol(LexicalUnit.ENDLINE,yyline,yycolumn,"\\n"));}




//Logical operatorss
"AND"		{return symbol(new Symbol(LexicalUnit.AND,yyline,yycolumn,yytext()));}
"OR"		{return symbol(new Symbol(LexicalUnit.OR,yyline,yycolumn,yytext()));}
"NOT"		{return symbol(new Symbol(LexicalUnit.NOT,yyline,yycolumn,yytext()));}
"IF"		{return symbol(new Symbol(LexicalUnit.IF,yyline,yycolumn,yytext()));}
"THEN"		{return symbol(new Symbol(LexicalUnit.THEN,yyline,yycolumn,yytext()));}
"ENDIF"		{return symbol(new Symbol(LexicalUnit.ENDIF,yyline,yycolumn,yytext()));}
"ELSE"		{return symbol(new Symbol(LexicalUnit.ELSE,yyline,yycolumn,yytext()));}
"WHILE"		{return symbol(new Symbol(LexicalUnit.WHILE,yyline,yycolumn,yytext()));}
"ENDWHILE"	{return symbol(new Symbol(LexicalUnit.ENDWHILE,yyline,yycolumn,yytext()));}
"DO"		{return symbol(new Symbol(LexicalUnit.DO,yyline,yycolumn,yytext()));}
"FOR"		{return symbol(new Symbol(LexicalUnit.FOR,yyline,yycolumn,yytext()));}
"ENDFOR"	{return symbol(new Symbol(LexicalUnit.ENDFOR,yyline,yycolumn,yytext()));}
"TO"	{return symbol(new Symbol(LexicalUnit.TO,yyline,yycolumn,yytext()));}


//fundamental functions
"PRINT" {return symbol(new Symbol(LexicalUnit.PRINT,yyline,yycolumn,yytext()));}
"READ" {return symbol(new Symbol(LexicalUnit.READ,yyline,yycolumn,yytext()));}
<<EOF>> {return symbol(new Symbol(LexicalUnit.EOS,yyline,yycolumn,yytext()));}


//numbers
{Integer}	{return symbol(new Symbol(LexicalUnit.NUMBER,yyline,yycolumn,yytext()));}


//variable identifier
{Identifier}  {return symbol(new Symbol(LexicalUnit.VARNAME,yyline,yycolumn,yytext()));}

.             {}
