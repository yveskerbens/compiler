package part2;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import part1.DataSource;
import part1.LexicalAnalyzer;
import part1.Symbol;
import part3.Interpretor;
import part3.Parser;

public class Main {
	public static void main(String[] args) throws FileNotFoundException  {
		DataSource  ds = DataSource.getInstance();
		Queue<Symbol> symbolTable = new LinkedList<Symbol>();
		Queue<Symbol> lexerSym = new LinkedList<Symbol>();
		String verbose = null;
		String treeArg = null;
		String outputArg = null;
		String treeFileName = null;
		String intOutPutFn = null;
	
		String[] argz = {args[0]};
		for (int i=1; i<=args.length-1; i++) {
	    	   if(args[i].equals("-v")) {
	    		   verbose = "-v";
	    	   } else if(args[i].equals("-t")) {
	    		   treeArg = "-t";
		    	   treeFileName = args[i+1];
		    	   System.out.println(args[i]);
		    	   System.out.println(args[i+1]);
		    	   
	    	   }else if(args[i].equals("-o")){
	    		   outputArg = "-o";
		    	   intOutPutFn   = args[i+1]; 
		    	   }
	    	   }  
		
	   ds.initializeTokensTable(); //this can be remove is using external lexer
       LexicalAnalyzer.main(argz);
       lexerSym  = ds.getSymbolTable();
       while (true) { //Removing the EOS symbol
    	   Symbol topQ =lexerSym.peek(); 
    	   if(topQ.isEOS()) {
    		  break;
    	   } 
    	   symbolTable.add(topQ);
    	   lexerSym.poll();
       } 
       if(verbose == "-v"){
    	   Parser.parser(symbolTable, verbose);
       }else {
    	   Parser.parser(symbolTable, null);		
       }
       
       // output the tree into a .tex file
       if(treeArg == "-t"){
	        ArrayList<String> fnSplitor = new ArrayList<String>( Arrays.asList(treeFileName.split("\\.")));
	        try {
	        	if(fnSplitor.get(1).equals("tex")) {
	        		System.out.print("Parse Tree Saved To: "+treeFileName+"\n\n");
	        		writeDataToFile(Parser.tree.toLaTeX(), treeFileName);
				}else {
					System.out.println("Unable to save your paretree due to file extension issue. \nFile name must a .tex extension");
				}
	        } catch (IndexOutOfBoundsException e) {
	        	System.out.println("Unable to save your paretree due to file extension issue \nFile name doesn't "
	        			+ "contain '.tex' extension");
	        	}
	        } //end of tree output processing
       
       //program intepretor
       if(Parser.accepted)
		{
			//output LLVM code
			if(args.length==1)
				System.out.print("Generated LLVM code:\n\n");
			//output LLVM code to .ll file
			if(args.length>2){
				if(outputArg == "-o"){
					System.out.print("LLVM code saved to : "+intOutPutFn+"\n\n");
					PrintStream ps=new PrintStream(new FileOutputStream(intOutPutFn));
					System.setOut(ps);}
			}
		Interpretor.genLLVM(Parser.tree.GetRoot());
		}
		else {
			System.err.println("Syntax Error!");
			} 
       } //end of tree programm interpretor processing
	
	private static void writeDataToFile(String data, String filename) {
        File file = new File(filename);
        FileWriter fr = null;
        try {
            fr = new FileWriter(file);
            fr.write(data);	
        } catch (IOException e) {
            e.printStackTrace();
        }finally{
            //close resources
            try {
                fr.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
	

		