package part2;

import java.util.ArrayList;
import java.util.Collections;

import part1.LexicalUnit;
/**
 * This class is production rule builder
 * allow to fabric production rule
 * @author yveskerbensdeclerus
 *
 */
public class ProductionRule {
	private Variable leftPart; 
	private SententialForm sf[];
	
	public ProductionRule(Variable leftPart, SententialForm[] productionRule) {
		this.leftPart = leftPart;
		this.sf = productionRule;
	}

	public ProductionRule() {
	
	}

	public Variable getLeftPart() {
		return leftPart;
	}

	public SententialForm[] getRightPart() {
		return sf;
	}
	
	
	public void displayRule(int depth) {
		StringBuilder sb = new StringBuilder();
		if(sf.length == 0) {sb.append("Ɛ");};
		for(int i=0; i<=sf.length-1; i++) {
			if(sf[i].getType() == "Variable") {
				sb.append("<");
				sb.append(sf[i].getV().toString());
				sb.append(">");
			} else if(sf[i].getType() == "Terminal")  {
                LexicalUnit T=sf[i].getT(); 
                switch(T)
                {
                    case BEGINPROG:sb.append("BEGINPROG");break;
                    case PROGNAME:sb.append("[ProgName]");break;
                    case VARIABLES:sb.append("VARIABLES");break;
                    case ENDPROG:sb.append(" ENDPROG");break;
                    case COMMA:sb.append(",");break;
                    case ASSIGN:sb.append(":=");break;
                    case LPAREN:sb.append("(");break;
                    case RPAREN:sb.append(")");break;
                    case MINUS:sb.append("-");break;
                    case ENDLINE:sb.append("[EndLine]"); break;
                    case PLUS:sb.append("+");break;
                    case TIMES:sb.append("*");break;
                    case DIVIDE:sb.append("/");break;
                    case IF:sb.append("IF");break;
                    case FOR:sb.append("FOR");break;
                    case THEN:sb.append("THEN");break;
                    case ENDIF:sb.append("ENDIF");break;
                    case ELSE:sb.append("ELSE");break;
                    case NOT:sb.append("NOT");break;
                    case AND:sb.append("AND");break;
                    case OR:sb.append("OR");break;
                    case EQ:sb.append("=");break;
                    case GEQ:sb.append(">=");break;
                    case GT:sb.append(">");break;
                    case LEQ:sb.append("<=");break;
                    case LT:sb.append("<");break;
                    case NEQ:sb.append("<>");break;
                    case WHILE:sb.append("WHILE");break;
                    case DO:sb.append("DO");break;
                    case TO:sb.append("TO");break;
                    case VARNAME:sb.append("[VarName]");break;
                    case NUMBER:sb.append("[Number]");break;
                    case PRINT:sb.append("PRINT");break;
                    case READ:sb.append("READ");break;
                    case EOS:sb.append("<<EOS>>");break;

                }
            } else {sb.append("$");}	
		}
		System.out.println(sb.toString());
		System.out.print(" (Derivation depth = "+depth+ " )");
        System.out.println();
	}	
}