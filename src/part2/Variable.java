package part2;

public enum Variable {
	S,
	Program,
	Variables,
	VarList,
	VarListP, 
	Code,
	Instruction, 
	Assign, 
    ExprArith,
    ExprAP,
    Prod,
    ProdP,
    Atom,
    OpG1, 
    OpG2,
    Cond, 
    CondP,
    CondBlock,
    CondBlockP,
    CondAtom,
    SimpleCond,
    BinOp,
    Comp, 
    If,
    IffSeq,
    While,
    For,
    Print,
    Read,
    ExpList,
    ExpListP,
    EndLine
}
