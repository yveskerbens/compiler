package part2;

import java.util.ArrayList;

import part1.LexicalUnit;

public class Grammar {
	private static ArrayList<ProductionRule> productionRuleList = new ArrayList<ProductionRule>();
	
	
	public Grammar() {
		productionRuleList.add(new ProductionRule(Variable.S,  
				new SententialForm[]{new SententialForm(Variable.Program), new SententialForm("$")})); //creating rule 0
		
		productionRuleList.add(new ProductionRule(Variable.Program, new SententialForm[] {new SententialForm(LexicalUnit.BEGINPROG), 
				new SententialForm(LexicalUnit.PROGNAME), new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine), new SententialForm(Variable.Variables), 
				new SententialForm(Variable.Code), new SententialForm(LexicalUnit.ENDPROG)}));  //creating rule 1
		
		productionRuleList.add(new ProductionRule(Variable.Variables,      //creating rule 2
				new SententialForm[]{new SententialForm(LexicalUnit.VARIABLES), new SententialForm(Variable.VarList),
						new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine)}));
		
		productionRuleList.add(new ProductionRule(Variable.Variables, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //creating rule 3
		
		productionRuleList.add(new ProductionRule(Variable.VarList, new SententialForm[] {new SententialForm(LexicalUnit.VARNAME), 
				new SententialForm(Variable.VarListP)})); //creating rule 4
		
		
		productionRuleList.add(new ProductionRule(Variable.VarListP,   //creating rule  5
				new SententialForm[]{new SententialForm(LexicalUnit.COMMA), new SententialForm(Variable.VarList)}));
		
		productionRuleList.add(new ProductionRule(Variable.VarListP, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //creating rule 6
		
		productionRuleList.add(new ProductionRule(Variable.Code, new SententialForm[] {new SententialForm(Variable.Instruction), 
				new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine), new SententialForm(Variable.Code) })); // creating rule 7
		
		productionRuleList.add(new ProductionRule(Variable.Code, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 8 
		
		productionRuleList.add(new ProductionRule(Variable.Instruction, new SententialForm[] {new SententialForm(Variable.Assign)})); //rule 9
		
		productionRuleList.add(new ProductionRule(Variable.Instruction, new SententialForm[] {new SententialForm(Variable.If)})); //rule 10
		
		productionRuleList.add(new ProductionRule(Variable.Instruction, new SententialForm[] {new SententialForm(Variable.While)})); //rule 11
		
		productionRuleList.add(new ProductionRule(Variable.Instruction, new SententialForm[] {new SententialForm(Variable.For)})); //rule 12
		
		productionRuleList.add(new ProductionRule(Variable.Instruction, new SententialForm[] {new SententialForm(Variable.Print)})); //rule 13
		
		productionRuleList.add(new ProductionRule(Variable.Instruction, new SententialForm[] {new SententialForm(Variable.Read)})); //rule 14
		
		productionRuleList.add(new ProductionRule(Variable.Assign, new SententialForm[] {new SententialForm(LexicalUnit.VARNAME),
				new SententialForm(LexicalUnit.ASSIGN), new SententialForm(Variable.ExprArith)})); //rule 15 
		
		productionRuleList.add(new ProductionRule(Variable.ExprArith,  new SententialForm[] { new SententialForm(Variable.Prod), 
				new SententialForm(Variable.ExprAP)} )); //rule 16
		
		productionRuleList.add(new ProductionRule(Variable.ExpListP, new SententialForm[] {new SententialForm(Variable.OpG1), new SententialForm(Variable.Prod),
				new SententialForm(Variable.ExprAP)})); //rule 17
		
		productionRuleList.add(new ProductionRule(Variable.ExprAP, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 18
		
		productionRuleList.add(new ProductionRule(Variable.Prod, new SententialForm[] {new SententialForm(Variable.Atom),
				new SententialForm(Variable.ProdP)})); //Rule 19
		
		productionRuleList.add(new ProductionRule(Variable.ProdP,  new SententialForm[] {new SententialForm(Variable.OpG2), 
				new SententialForm(Variable.Atom), new SententialForm(Variable.ProdP)})); //rule 20
		
		productionRuleList.add(new ProductionRule(Variable.ProdP, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 21
		
		productionRuleList.add(new ProductionRule(Variable.Atom, new SententialForm[] {new SententialForm(LexicalUnit.MINUS),
				new SententialForm(Variable.Atom)})); //rule 22
		
		productionRuleList.add(new ProductionRule(Variable.Atom, new SententialForm[] {new SententialForm(LexicalUnit.VARNAME)})); //rule 23
		
		productionRuleList.add(new ProductionRule(Variable.Atom, new SententialForm[] {new SententialForm(LexicalUnit.NUMBER)}));//rule 24
		
		productionRuleList.add(new ProductionRule(Variable.Atom, new SententialForm[] {new SententialForm(LexicalUnit.LPAREN),
				new SententialForm(Variable.ExprArith), new SententialForm(LexicalUnit.RPAREN)})); //rule 25
		
		productionRuleList.add(new ProductionRule(Variable.OpG1, new SententialForm[] {new SententialForm(LexicalUnit.PLUS)})); //rule 26
		
		productionRuleList.add(new ProductionRule(Variable.OpG1, new SententialForm[] {new SententialForm(LexicalUnit.MINUS)})); //rule 27
		
		productionRuleList.add(new ProductionRule(Variable.OpG2, new SententialForm[] {new SententialForm(LexicalUnit.TIMES)})); //rule 28
		
		productionRuleList.add(new ProductionRule(Variable.OpG2, new SententialForm[] {new SententialForm(LexicalUnit.DIVIDE)})); //rule 29
		
		productionRuleList.add(new ProductionRule(Variable.If, new SententialForm[] {new SententialForm(LexicalUnit.IF),  //rule 30
				new SententialForm(LexicalUnit.LPAREN), new SententialForm(Variable.Cond), new SententialForm(LexicalUnit.RPAREN),
				 new SententialForm(LexicalUnit.THEN),  new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine), new SententialForm(Variable.Code),
						 new SententialForm(Variable.IffSeq)}));
		
		productionRuleList.add(new ProductionRule(Variable.IffSeq, new SententialForm[] {new SententialForm(LexicalUnit.ENDIF)})); //rule 31
		
		productionRuleList.add(new ProductionRule(Variable.IffSeq, new SententialForm[] {new SententialForm(LexicalUnit.ELSE), 
				new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine), new SententialForm(Variable.Code), new SententialForm(LexicalUnit.ENDIF)})); //rule 32
		
		productionRuleList.add(new ProductionRule(Variable.Cond, new SententialForm[] {new SententialForm(Variable.CondBlock), //rule 33
				new SententialForm(Variable.CondP)}));
		
		productionRuleList.add(new ProductionRule(Variable.CondP, new SententialForm[] {new SententialForm(LexicalUnit.OR), //rule 34
				new SententialForm(Variable.CondBlock), new SententialForm(Variable.CondP)}));
		
		productionRuleList.add(new ProductionRule(Variable.CondP, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 35
		
		productionRuleList.add(new ProductionRule(Variable.CondBlock, new SententialForm[] {new SententialForm(Variable.CondAtom), //rule 36
				new SententialForm(Variable.CondBlockP)}));
		
		productionRuleList.add(new ProductionRule(Variable.CondBlockP, new SententialForm[] {new SententialForm(LexicalUnit.AND), //rule 37
				new SententialForm(Variable.CondAtom), new SententialForm(Variable.CondBlockP)}));
		
		productionRuleList.add(new ProductionRule(Variable.CondBlockP, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 38
		
		productionRuleList.add(new ProductionRule(Variable.CondAtom, new SententialForm[] {new SententialForm(LexicalUnit.NOT), //rule 39
				new SententialForm(Variable.SimpleCond)}));
		
		productionRuleList.add(new ProductionRule(Variable.CondAtom, new SententialForm[] {new SententialForm(Variable.SimpleCond)})); //rule 40
		
		productionRuleList.add(new ProductionRule(Variable.SimpleCond, new SententialForm[] {new SententialForm(Variable.ExprArith), //rule 41
				new SententialForm(Variable.Comp), new SententialForm(Variable.ExprArith)})); 
		
		productionRuleList.add(new ProductionRule(Variable.Comp, new SententialForm[] {new SententialForm(LexicalUnit.EQ)})); //rule 42
		
		productionRuleList.add(new ProductionRule(Variable.Comp, new SententialForm[] {new SententialForm(LexicalUnit.GEQ)})); //rule 43
		
		productionRuleList.add(new ProductionRule(Variable.Comp, new SententialForm[] {new SententialForm(LexicalUnit.GT)})); //rule 44
		
		productionRuleList.add(new ProductionRule(Variable.Comp, new SententialForm[] {new SententialForm(LexicalUnit.LEQ)})); //rule 45
		
		productionRuleList.add(new ProductionRule(Variable.Comp, new SententialForm[] {new SententialForm(LexicalUnit.LT)})); //rule 46
		
		productionRuleList.add(new ProductionRule(Variable.Comp, new SententialForm[] {new SententialForm(LexicalUnit.NEQ)})); //rule 47
		
		productionRuleList.add(new ProductionRule(Variable.While, new SententialForm[] {new SententialForm(LexicalUnit.WHILE), //rule 48
				new SententialForm(LexicalUnit.LPAREN),new SententialForm(Variable.Cond), new SententialForm(LexicalUnit.RPAREN), 
				new SententialForm(LexicalUnit.DO), new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine), new SententialForm(Variable.Code), 
				new SententialForm(LexicalUnit.ENDWHILE)})); 
		
		productionRuleList.add(new ProductionRule(Variable.For, new SententialForm[] {new SententialForm(LexicalUnit.FOR), //rule 49
				new SententialForm(LexicalUnit.VARNAME),new SententialForm(LexicalUnit.ASSIGN), new SententialForm(Variable.ExprArith), 
				new SententialForm(LexicalUnit.TO), new SententialForm(Variable.ExprArith), new SententialForm(LexicalUnit.DO), new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine),
				new SententialForm(Variable.Code), new SententialForm(LexicalUnit.ENDFOR)}));
		
		productionRuleList.add(new ProductionRule(Variable.Print, new SententialForm[] {new SententialForm(LexicalUnit.PRINT), //rule 50
				new SententialForm(LexicalUnit.LPAREN), new SententialForm(Variable.ExpList), new SententialForm(LexicalUnit.RPAREN)}));
		
		productionRuleList.add(new ProductionRule(Variable.Read, new SententialForm[] {new SententialForm(LexicalUnit.READ), //rule 51
				new SententialForm(LexicalUnit.LPAREN), new SententialForm(Variable.VarList), new SententialForm(LexicalUnit.RPAREN)}));
	
		
		productionRuleList.add(new ProductionRule(Variable.ExpList, new SententialForm[] {new SententialForm(Variable.ExprArith),  //rule 52
				new SententialForm(Variable.ExpListP)})); 
		
		productionRuleList.add(new ProductionRule(Variable.ExpListP, new SententialForm[] {new SententialForm(LexicalUnit.COMMA), //rule 53
				new SententialForm(Variable.ExpList)})); 
		
		productionRuleList.add(new ProductionRule(Variable.ExpListP, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 54
		
		productionRuleList.add(new ProductionRule(Variable.EndLine, new SententialForm[] {new SententialForm(LexicalUnit.ENDLINE), new SententialForm(Variable.EndLine)})); //rule 55
		
		productionRuleList.add(new ProductionRule(Variable.EndLine, new SententialForm[] {new SententialForm(LexicalUnit.EPSILON)})); //rule 56
		
		productionRuleList.add(new ProductionRule(Variable.Program, new SententialForm[] {new SententialForm(Variable.EndLine),new SententialForm(Variable.Program)}));
		
	}
	
	public static ArrayList<ProductionRule> getProductionRuleList() {
		return productionRuleList;
	}
	
	
	public static void displayProductRule(){
		System.out.println("Super-Fortan Grammar Action Table");
		for (int i=0; i<=productionRuleList.size()-1; i++) {
			System.out.print(String.format("%-18s-->%s", "["+i+"]" + "<"+productionRuleList.get(i).getLeftPart().toString() + ">" ," " ));
		}
	}	
}
