package part2;

public class ParseTree {
    private Node RootNode;
    private Node ChildNode;
   

    public ParseTree(SententialForm SF)
    {
        this.RootNode = new Node(SF,null);
        this.ChildNode = this.RootNode;
    }
 
    /**
     * Ths fuction takes the right part of a production rule 
     * @param stf
     */
    public void addChildrenNode(SententialForm[] stf, String verbose)
    {
    	if(stf.length >0 ) {
    		 this.ChildNode.AddChildren(stf, verbose);
    	} 
    }
    
    public void findNextChildren(String verbose)  //find the next left-most SententialForm, set current=null if it finish
    {

        if(this.ChildNode.GetSententialForm().getType()=="Variable" && this.ChildNode.GetNumberofChildren()>0)  //rule is produced
        {
        	this.ChildNode  = this.ChildNode.GetNextChild(verbose);
        }
        else
        { 
           while(ChildNode.getStack().isEmpty())
            {
                // back to parent level
                this.ChildNode = this.ChildNode.GetParent();
               
                if(this.ChildNode == this.RootNode && this.ChildNode.getStack().isEmpty())
                {
                    this.ChildNode = null;
                    return;
                } 
            } 
         
            this.ChildNode  = this.ChildNode.GetNextChild(verbose);
        }
    }

    public Node GetRoot()
    {
        return this.RootNode;
    }

    public Node GetChildNode()
    {
        return this.ChildNode;
    }

    public Boolean isFinish()
    {
        return (this.ChildNode == null);
    }

    public void SetTerminalValue(Object Value)
    {
        this.ChildNode.setTerminal(Value);
    }
    
    public String toLaTeX() {
    	return RootNode.toLaTeX();
    }
    
}
