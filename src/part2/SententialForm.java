
package part2;
import part1.LexicalUnit;;

/**
 * defining sentential to be used by production rule.
 * @author yveskerbensdeclerus
 *
 */
public class SententialForm {
	private Variable  V; 
	private LexicalUnit T;
	private String type;
	private String str;
	private  Object value;

	public  SententialForm(Variable v) {
		this.V = v;
		this.type = "Variable";
	}
	
	public  SententialForm(LexicalUnit t) {
		this.T = t;
		this.type = "Terminal";
	}
	
	public SententialForm(String str) {
		this.str = str;
	}

	public Variable getV() {
		return V;
	}

	public LexicalUnit getT() {
		return T;
	}

	public String getType() {
		return type;
	}

	public String getStr() {
		return str;
	}	
	
	public boolean isEpsilon(){
		return this.type.equals(LexicalUnit.EPSILON);
		}
	
	public boolean isEOS(){
	        return this.type.equals(LexicalUnit.EOS);
	}
	
	public String toTeX(){
        if(this.type == "Terminal"){
            final String value = this.T != null? this.T.toString() : "";
            final String type  = this.type  != null? this.type.toString()  : "";
            return type+" \\texttt{"+value+"}";
        }
        
        else if (this.isEpsilon()){
            return "$\\varepsilon$";
        }
        else if (this.isEOS()){
            return "$\\dashv$";
        }
        else {
            return this.V.toString();
        }
    }	
}
