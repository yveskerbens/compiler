package part2;

import java.util.HashMap;
/**
 * Defining the action table for the grammar. 
 * @author yveskerbensdeclerus
 * 
 */

import part1.LexicalUnit;

public class ActionTable {
	private static  HashMap<Variable, HashMap<Object, Integer>> actionTableMap = 
			new  HashMap<Variable, HashMap<Object, Integer>> ();

	public ActionTable() {
		addItemToActionTable(Variable.Program, LexicalUnit.BEGINPROG, 1);
		addItemToActionTable(Variable.Program, LexicalUnit.ENDLINE, 57);
		addItemToActionTable(Variable.Variables, "$", 3);
		addItemToActionTable(Variable.Variables, LexicalUnit.VARIABLES, 2);
		addItemToActionTable(Variable.Variables, LexicalUnit.VARNAME, 3);
		addItemToActionTable(Variable.Variables, LexicalUnit.IF, 3);
		addItemToActionTable(Variable.Variables, LexicalUnit.WHILE, 3);
		addItemToActionTable(Variable.Variables, LexicalUnit.FOR, 3);
		addItemToActionTable(Variable.Variables, LexicalUnit.PRINT, 3);
		addItemToActionTable(Variable.Variables, LexicalUnit.READ, 3);
		addItemToActionTable(Variable.VarList, LexicalUnit.VARNAME, 4);
		addItemToActionTable(Variable.VarListP, LexicalUnit.COMMA, 5);
		addItemToActionTable(Variable.VarListP, LexicalUnit.RPAREN, 6);
		addItemToActionTable(Variable.VarListP, LexicalUnit.ENDLINE, 6);
		addItemToActionTable(Variable.Code, LexicalUnit.VARNAME, 7);
		addItemToActionTable(Variable.Code, LexicalUnit.IF, 7);
		addItemToActionTable(Variable.Code, LexicalUnit.FOR, 7);
		addItemToActionTable(Variable.Code, LexicalUnit.PRINT, 7);
		addItemToActionTable(Variable.Code, LexicalUnit.READ, 7);
		addItemToActionTable(Variable.Code, LexicalUnit.WHILE, 7);
		addItemToActionTable(Variable.Code, LexicalUnit.ENDPROG, 8);
		addItemToActionTable(Variable.Code, LexicalUnit.ENDIF, 8);
		addItemToActionTable(Variable.Code, LexicalUnit.ELSE, 8);
		addItemToActionTable(Variable.Code, LexicalUnit.ENDWHILE, 8);
		addItemToActionTable(Variable.Code, LexicalUnit.ENDFOR, 8);
		addItemToActionTable(Variable.Instruction, LexicalUnit.VARNAME, 9);
		addItemToActionTable(Variable.Instruction, LexicalUnit.IF, 10);
		addItemToActionTable(Variable.Instruction, LexicalUnit.WHILE, 11);
		addItemToActionTable(Variable.Instruction, LexicalUnit.FOR, 12);
		addItemToActionTable(Variable.Instruction, LexicalUnit.PRINT, 13);
		addItemToActionTable(Variable.Instruction, LexicalUnit.READ, 14);
		addItemToActionTable(Variable.Assign, LexicalUnit.VARNAME, 15);
		addItemToActionTable(Variable.ExprArith, LexicalUnit.VARNAME, 16);
		addItemToActionTable(Variable.ExprArith, LexicalUnit.LPAREN, 16);
		addItemToActionTable(Variable.ExprArith, LexicalUnit.MINUS, 16);
		addItemToActionTable(Variable.ExprArith, LexicalUnit.NUMBER, 16);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.PLUS, 17);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.MINUS, 17);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.ENDLINE, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.AND, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.OR, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.EQ, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.GEQ, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.GT, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.LEQ, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.LT, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.NEQ, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.DO, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.TO, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.RPAREN, 18);
		addItemToActionTable(Variable.ExprAP, LexicalUnit.COMMA, 18);
		addItemToActionTable(Variable.Prod, LexicalUnit.LPAREN, 19);
		addItemToActionTable(Variable.Prod, LexicalUnit.PLUS, 19);
		addItemToActionTable(Variable.Prod, LexicalUnit.MINUS, 19);
		addItemToActionTable(Variable.Prod, LexicalUnit.NUMBER, 19);
		addItemToActionTable(Variable.Prod, LexicalUnit.VARNAME, 19);
		addItemToActionTable(Variable.ProdP, LexicalUnit.TIMES, 20);
		addItemToActionTable(Variable.ProdP, LexicalUnit.DIVIDE, 20);
		addItemToActionTable(Variable.ProdP, LexicalUnit.ENDLINE, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.PLUS, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.MINUS, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.AND, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.OR, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.EQ, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.GEQ, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.GT, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.LEQ, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.LT, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.NEQ, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.DO, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.TO, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.RPAREN, 21);
		addItemToActionTable(Variable.ProdP, LexicalUnit.COMMA, 21);
		addItemToActionTable(Variable.Atom, LexicalUnit.VARNAME, 23);
		addItemToActionTable(Variable.Atom, LexicalUnit.MINUS, 22);
		addItemToActionTable(Variable.Atom, LexicalUnit.NUMBER, 24);
		addItemToActionTable(Variable.Atom, LexicalUnit.LPAREN, 25);
		addItemToActionTable(Variable.OpG1, LexicalUnit.PLUS, 26);
		addItemToActionTable(Variable.OpG1, LexicalUnit.MINUS, 27);
		addItemToActionTable(Variable.OpG2, LexicalUnit.TIMES, 28);
		addItemToActionTable(Variable.OpG2, LexicalUnit.DIVIDE, 29);
		addItemToActionTable(Variable.If, LexicalUnit.IF, 30);
		addItemToActionTable(Variable.IffSeq, LexicalUnit.ENDIF, 31);	
		addItemToActionTable(Variable.IffSeq, LexicalUnit.ELSE, 32);
		addItemToActionTable(Variable.Cond, LexicalUnit.NOT, 33);
		addItemToActionTable(Variable.Cond, LexicalUnit.VARNAME, 33);	
		addItemToActionTable(Variable.Cond, LexicalUnit.MINUS, 33);
		addItemToActionTable(Variable.Cond, LexicalUnit.NUMBER, 33);
		addItemToActionTable(Variable.Cond, LexicalUnit.LPAREN, 33);
		addItemToActionTable(Variable.CondP, LexicalUnit.AND, 34);
		addItemToActionTable(Variable.CondP, LexicalUnit.RPAREN, 35);
		addItemToActionTable(Variable.CondBlock, LexicalUnit.NOT, 36);
		addItemToActionTable(Variable.CondBlock, LexicalUnit.VARNAME, 36);	
		addItemToActionTable(Variable.CondBlock, LexicalUnit.MINUS, 36);
		addItemToActionTable(Variable.CondBlock, LexicalUnit.NUMBER, 36);
		addItemToActionTable(Variable.CondBlock, LexicalUnit.LPAREN, 36);
		addItemToActionTable(Variable.CondBlockP, LexicalUnit.AND, 37);
		addItemToActionTable(Variable.CondBlockP, LexicalUnit.RPAREN, 38);
		addItemToActionTable(Variable.CondAtom, LexicalUnit.NOT, 39);
		addItemToActionTable(Variable.CondAtom, LexicalUnit.VARNAME, 40);	
		addItemToActionTable(Variable.CondAtom, LexicalUnit.MINUS, 40);
		addItemToActionTable(Variable.CondAtom, LexicalUnit.NUMBER, 40);
		addItemToActionTable(Variable.CondAtom, LexicalUnit.LPAREN, 40);
		addItemToActionTable(Variable.SimpleCond, LexicalUnit.VARNAME, 41);	
		addItemToActionTable(Variable.SimpleCond, LexicalUnit.MINUS, 41);
		addItemToActionTable(Variable.SimpleCond, LexicalUnit.NUMBER, 41);
		addItemToActionTable(Variable.SimpleCond, LexicalUnit.LPAREN, 41);
		addItemToActionTable(Variable.SimpleCond, LexicalUnit.NOT, 41);
		addItemToActionTable(Variable.Comp, LexicalUnit.EQ, 42);
		addItemToActionTable(Variable.Comp, LexicalUnit.GEQ, 43);
		addItemToActionTable(Variable.Comp, LexicalUnit.GT, 44);
		addItemToActionTable(Variable.Comp, LexicalUnit.LEQ, 45);
		addItemToActionTable(Variable.Comp, LexicalUnit.LT, 46);
		addItemToActionTable(Variable.Comp, LexicalUnit.NEQ, 47);
		addItemToActionTable(Variable.While, LexicalUnit.WHILE, 48);
		addItemToActionTable(Variable.For, LexicalUnit.FOR, 49);
		addItemToActionTable(Variable.Print, LexicalUnit.PRINT, 50);
		addItemToActionTable(Variable.Read, LexicalUnit.READ, 51);
		addItemToActionTable(Variable.ExpList, LexicalUnit.VARNAME, 52);
		addItemToActionTable(Variable.ExpList, LexicalUnit.MINUS, 52);
		addItemToActionTable(Variable.ExpList, LexicalUnit.NUMBER, 52);
		addItemToActionTable(Variable.ExpList, LexicalUnit.LPAREN, 52);
		addItemToActionTable(Variable.ExpListP, LexicalUnit.COMMA, 53);
		addItemToActionTable(Variable.ExpListP, LexicalUnit.RPAREN, 54);
		addItemToActionTable(Variable.EndLine, LexicalUnit.ENDLINE, 55);
		addItemToActionTable(Variable.EndLine, LexicalUnit.VARIABLES, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.VARNAME, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.IF, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.WHILE, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.FOR, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.PRINT, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.READ, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.ENDPROG, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.ENDIF, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.ENDWHILE, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.ENDFOR, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.ELSE, 56);
		addItemToActionTable(Variable.EndLine, LexicalUnit.BEGINPROG, 56);	
	}
/**
 * allow  to add  variables, first  and action to the action table
 * @param var this is the variable to be at the top of the stack
 * @param lu this is  the first of or follow of
 * @param i rules number
 */
	private  void addItemToActionTable(Variable var, Object lu, Integer i) {
		HashMap<Object, Integer> tff;
		if(!actionTableMap.containsKey(var)) {
			tff = new HashMap<Object, Integer>();
			tff.put(lu, i);
			actionTableMap.put(var, tff);	
		} else {
			tff = actionTableMap.get(var);
			tff.put(lu, i);
		}	
	}	
	
	public static HashMap<Variable, HashMap<Object, Integer>> getActionTableMap() {
		return actionTableMap;
	}

	public void displayActionTable() {
		for(Variable v: actionTableMap.keySet()) {
			System.out.println(v.toString());
			System.out.print(": ");
			for (Object lu: actionTableMap.get(v).keySet()) {
				System.out.print("(");
				System.out.print(lu.toString() + "  " + actionTableMap.get(v).get(lu).toString());
				System.out.print(")");
				System.out.print("  ");
			}
			System.out.println();
		}
	}
	
	/**
	 * This method receiving a variables and a first of or follow of and return the next rule number
	 * @param var
	 * @param lu
	 * @return
	 */
	public static int  getProductionRule(Variable var, LexicalUnit lu) {
		if(actionTableMap.containsKey(var)) {
			HashMap<Object, Integer> rule = actionTableMap.get(var);
			if(rule.containsKey(lu)) {
				int ruleNumber = rule.get(lu);
				return ruleNumber;
			}
		}
		return -1; 
	}
}
