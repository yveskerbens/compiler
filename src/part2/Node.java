package part2;

import java.util.ArrayList;
import java.util.Iterator;

import part1.LexicalUnit;
/**
 * The node has
 * a name (the same as the variable name)
 * a parent
 * and a List of children
 * thus a node without children is a leaf
 */
public class Node {
    private SententialForm  SentialF;
    private Node ParentNode;
    private ArrayList<Node> nodeChildren;
    private Object Terminal;
    private  ArrayList<Node>stack = new ArrayList<Node> ();
    Iterator<Node> itpopnextchild ;
   
    public Node(SententialForm  Ele, Node ParentNode){
        this.SentialF=Ele;
        this.ParentNode=ParentNode;
        this.nodeChildren = new ArrayList<Node> (); 
    }
/**
 * adding a chld node along with it's parent node
 * @param SententialForm
 */
    public   void AddChildren(SententialForm [] SententialForm, String verbose){
        Node TargetNode=this;
  
        for(int i=0;i<SententialForm.length;++i){
        	SententialForm  sf = SententialForm[i];
        	Node ChildrentoAdd=new Node(sf,this);
            if(sf.getType() == "Variable") {
            	TargetNode.nodeChildren.add(ChildrentoAdd);
            	if(verbose != null) {
            		System.out.println("Added " + ChildrentoAdd.SentialF.getV() + " to Parent Node " + this.SentialF.getV());
            	}
            }else {
            	if(!sf.getT().equals(LexicalUnit.EPSILON)) {
            		TargetNode.nodeChildren.add(ChildrentoAdd);
            		if(verbose != null) {
                		System.out.println("Added " + ChildrentoAdd.SentialF.getT() + " to Parent Node " + this.SentialF.getV());
                	}
            	}
            }
            TargetNode.stack.add(ChildrentoAdd);
        }
    }

    public int GetNumberofChildren(){
    	Node TargetNode=this;
        return TargetNode.nodeChildren.size();
    }

    public Node GetSpecificChild(int position){
    	Node TargetNode=this;
    	return TargetNode.nodeChildren.get(position);
    }

    public void setTerminal(Object Value){
        this.Terminal=Value;
    }

    public Node GetParent()
    {
        return this.ParentNode;
    }

    public Object GetTerminal(){
        return this.Terminal;
    }
/**
 * 
 * @return
 */
    public Node GetNextChild(String verbose){
    	itpopnextchild  =stack.iterator();
    	if(itpopnextchild.hasNext()) {
    		Node n = itpopnextchild.next();
    		stack.remove(n); //pop element from the stack
    		if(verbose!=null) {
    			if(n.SentialF.getType().equals("Variable")) {
    				System.out.println("Pop " + n.SentialF.getV() + " from the stack");
    			}else {
    				System.out.println("Pop " + n.SentialF.getT() + " from the stack");
    			}
    		}
    		return n;
    	} else {
    		return this;
    	}
    }
    
    public ArrayList<Node> getStack() {
		return stack;
	}
    
	public ArrayList<Node> getNodeChildren() {
		return nodeChildren;
	}

	public SententialForm GetSententialForm (){
        return this.SentialF;
    }
	
	  /** Writes the tree as LaTeX code.
     */
    public String toTeX() {
    	Node Node =this;
        StringBuilder treeTeX = new StringBuilder();
        treeTeX.append("[.");
        if (Node.SentialF.isEpsilon()){
            treeTeX.append("$\\varepsilon$");
        }
        else {
            treeTeX.append(this.SentialF);
        }
        treeTeX.append(" ");
        for (Node child: this.nodeChildren) {
            treeTeX.append(child.toTeX());
                }
        treeTeX.append("]");
        return treeTeX.toString();
    }

    /** Writes the tree as TikZ code.
     *  TikZ is a language to specify drawings in LaTeX files.
     */
    public String toTikZ() {
    	Node Node =this;
        StringBuilder treeTikZ = new StringBuilder();
        treeTikZ.append("node {");
        treeTikZ.append(Node.SentialF.toTeX());
        treeTikZ.append("}\n");
        for (Node child: Node.nodeChildren) {
            treeTikZ.append("child { ");
            treeTikZ.append(child.toTikZ());
            treeTikZ.append(" }\n");
        }
        return treeTikZ.toString();
    }

    /** Writes the tree as a TikZ picture.
     *  A TikZ picture embeds TikZ code so that LaTeX undertands it.
     */
    public String toTikZPicture() {
        return "\\begin{tikzpicture}[tree layout]\n\\" + toTikZ() + ";\n\\end{tikzpicture}";
    }

    /** Writes the tree as a LaTeX document which can be compiled (using the LuaLaTeX engine).
     *  Be careful that such code will not compile with PDFLaTeX,
     *  since the tree drawing algorithm is written in Lua.
     *  The code is not very readable as such, but you can have a look at the outputted file
     *  if you want to understand better.
     */
    public String toLaTeX() {
        return "\\RequirePackage{luatex85}\n\\documentclass{standalone}\n\n\\usepackage{tikz}\n\n\\usetikzlibrary{graphdrawing, graphdrawing.trees}\n\n\\begin{document}\n\n" + toTikZPicture() + "\n\n\\end{document}\n%% Local Variables:\n%% TeX-engine: luatex\n%% End:";
    }

}
